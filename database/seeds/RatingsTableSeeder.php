<?php

use Illuminate\Database\Seeder;
use App\Models\Rating;
use Illuminate\Support\Facades\DB;

class RatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach (range(1, 30) as $value){
            DB::table('ratings')->insert([
                'rating' => rand(1,5),
                'user_id' => rand(1,10),
                'product_id' => rand(1, 30)
            ]);
        }
    }
}
