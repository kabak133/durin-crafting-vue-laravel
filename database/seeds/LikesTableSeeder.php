<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1, 20) as $index){
            DB::table('likes')->insert([
//                'like' => 1,
                'posted_at' => $faker->date(),
                'post_id' => $index,
                'user_id' => rand(1, 10)
            ]);
        }
    }
}
