<?php

use Faker\Generator as Faker;

$user = \App\Models\User::where('is_admin', 1)->first();

$factory->define(App\Models\Post::class, function (Faker $faker) use ($user) {
    return [
           'title' => $faker->word,
           'body' => $faker->text(),
        'user_id' => $user->id
    ];
});