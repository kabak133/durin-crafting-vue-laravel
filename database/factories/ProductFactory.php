<?php

use Faker\Generator as Faker;

$user = \App\Models\User::where('is_admin', 1)->first();

$factory->define(\App\Models\Product::class, function (Faker $faker) use ($user) {
    return [
        'name' => $faker->name,
        'price' => rand(10, 100),
        'description' => $faker->text(),
        'add_info' => $faker->sentence(6, true),
        'short_description' => $faker->sentence(6, true),
        'property' => $faker->text(),
        'user_id' => $user->id
    ];
});