<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Comment::class, function (Faker $faker){
    return [
        'body' => $faker->text(),
        'commentable_id' => rand(1, 30),
        'commentable_type' => rand(0, 1) == 1 ? 'App\Models\Post' : 'App\Models\Product',
        'user_id' => rand(1, 10)
    ];
});