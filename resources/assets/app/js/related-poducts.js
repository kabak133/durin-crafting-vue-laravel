let relatedproducts = [
    {
        id: 1,
        name: 'Metal Bowl',
        price: 99,
        category_id:1,
        category: 'BOWLS',
        images: [
            require('../img/uploads/1.png')
        ],
        sale: true,
        oldPrice: 100

    },
    {
        id: 2,
        name: 'Metal Bowl 2',
        price: 89,
        category_id: 1,
        category: 'BOWLS',
        images: [
            require('../img/uploads/2.png')
        ],
        sale: false

    },
    {
        id: 3,
        name: 'Metal Bowl 3',
        price: 56,
        category_id: 2,
        category: 'Spoons',
        images: [
            require('../img/uploads/3.png')
        ],
        oldPrice: 100,
        sale: true

    },
    {
        id: 4,
        name: 'Metal Bowl 4',
        price: 66,
        category_id: 2,
        category: 'Spoons',
        images: [
            require('../img/uploads/3.png')
        ],
        sale: false

    },
    {
        id: 5,
        name: 'Metal Bowl 5',
        price: 78,
        category_id: 3,
        category: 'Forks',
        images: [
            require('../img/uploads/3.png')
        ],
        oldPrice: 100,
        sale: true

    }


];
export default relatedproducts;
