import Vue from 'vue';

Vue.filter('cropText', function (str, count) {
    if(str && count){
        if(str.length > count){
            return str.slice(0, count) + '...'
        }else {
            return str
        }

    }

});
