let products = [
    {
        id: 1,
        name: 'Metal Bowl',
        price: 99,
        images: [
            require('../img/uploads/1.png'),
            require('../img/uploads/2.png'),
            require('../img/uploads/3.png'),
        ],
        shortDescription: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque',
        properties: `• Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate
                    • Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate`,
        rating: 4,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
                    eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                    mollit anim id est laborum`,
        aditionalInfo: `aditionalInfo Lorem ipsum dolor sit amet, consectetur adipiscing`,
        reviews: [
            {
                author: 'Dima',
                avatar: require('../img/uploads/ava2.png'),
                text: 'Supper Loжka',
                date: '01.02.2018',
                rating: 3,

            },
            {
                author: 'Igor',
                avatar: require('../img/uploads/vatar.png'),
                text: 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit',
                date: '06.02.2018',
                rating: 5,

            },

        ]

    },
    {
        id: 2,
        name: 'Metal Bowl 2()',
        price: 89,
        images: [
            require('../img/uploads/3.png'),
            require('../img/uploads/2.png'),
            require('../img/uploads/1.png'),
        ],
        shortDescription: '22 Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque',
        properties: `• Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate
                    • Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate`,
        rating: 5,
        description: `2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
                    eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                    mollit anim id est laborum`,
        aditionalInfo: `2 aditionalInfo Lorem ipsum dolor sit amet, consectetur adipiscing`,
        reviews: []

    },
    {
        id: 3,
        name: 'Metal Bowl 3',
        price: 56,
        images: [
            require('../img/uploads/3.png'),
            require('../img/uploads/2.png'),
            require('../img/uploads/1.png'),
        ],
        shortDescription: '22 Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque',
        properties: `• Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate
                    • Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate`,
        rating: 4,
        description: `2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
                    eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                    mollit anim id est laborum`,
        aditionalInfo: `2 aditionalInfo Lorem ipsum dolor sit amet, consectetur adipiscing`,
        reviews: []

    },{
        id: 4,
        name: 'Metal Bowl 4',
        price: 66,
        images: [
            require('../img/uploads/3.png'),
            require('../img/uploads/2.png'),
            require('../img/uploads/1.png'),
        ],
        shortDescription: '22 Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque',
        properties: `• Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate
                    • Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate`,
        rating: 4,
        description: `2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
                    eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                    mollit anim id est laborum`,
        aditionalInfo: `2 aditionalInfo Lorem ipsum dolor sit amet, consectetur adipiscing`,
        reviews: []

    },
    {
        id: 5,
        name: 'Metal Bowl 5',
        price: 78,
        images: [
            require('../img/uploads/3.png'),
            require('../img/uploads/2.png'),
            require('../img/uploads/1.png'),
        ],
        shortDescription: '22 Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque',
        properties: `• Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate
                    • Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit
                    • Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
                    • Ut enim ad minima veniam, quis nostrum exercitationem
                    • Quis autem vel eum iure reprehenderit qui in ea voluptate`,
        rating: 4,
        description: `2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore 
                    eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                    mollit anim id est laborum`,
        aditionalInfo: `2 aditionalInfo Lorem ipsum dolor sit amet, consectetur adipiscing`,
        reviews: []

    },

];
export default products;
