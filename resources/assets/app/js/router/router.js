import Vue from 'vue'
import Router from 'vue-router'

const home = () => import ('../views/HomePage'),
  blog = () => import ('../views/BlogPage'),
  singlePost = () => import ('../views/SinglePostPage'),
  about = () => import ('../views/AboutUsPage'),
  NotFoundComponent = () => import ('../views/NotFoundComponent'),
  ContactPage = () => import ('../views/ContactPage'),
  SimpleProductPage = () => import ('../views/SimpleProductPage'),
  ProductsListPage = () => import ('../views/ProductsListPage')

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior() {
    return {x: 0, y: 0}
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/blog',
      name: 'blog',
      component: blog,
    },
    {
      path: '/post/:slug',
      name: 'singlePost',
      component: singlePost
    },
    {
      path: '/about-us',
      name: 'about-us',
      component: about
    },
    {
      path: '/contact-us',
      name: 'contact-us',
      component: ContactPage
    },
    {
      path: '/product/:slug',
      name: 'product',
      component: SimpleProductPage
    },
    {
      path: '/products/:price?:category?:sale?',
      name: 'products',
      component: ProductsListPage
    },
    {
      path: '*',
      component: NotFoundComponent
    }
  ]
})
