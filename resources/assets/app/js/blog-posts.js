let posts = [
    {
        id: 1,
        title: '1 Why the use of metal dishes is much more useful for health?',
        img: require('../img/uploads/1.png'),
        date: '09.07.2018',
        desc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco laboris nisi ut aliquip ex `,
        sumLikes: 5,
        sumComments: 10,
        like: false

    }, {
        id: 2,
        title: '2 Why the use of metal dishes is much more useful for health?',
        img: require('../img/uploads/1.png'),
        date: '09.07.2018',
        desc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco laboris nisi ut aliquip ex `,
        sumLikes: 0,
        sumComments: 0,
        like: false

    }, {
        id: 3,
        title: '3 Why the use of metal dishes is much more useful for health?',
        img: require('../img/uploads/1.png'),
        date: '09.07.2018',
        desc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco laboris nisi ut aliquip ex `,
        sumLikes: 13,
        sumComments: 45,
        like: true

    }, {
        id: 4,
        title: '4 Why the use of metal dishes is much more useful for health?',
        img: require('../img/uploads/1.png'),
        date: '09.07.2018',
        desc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco laboris nisi ut aliquip ex `,
        sumLikes: 752,
        sumComments: 3,
        like: true

    }, {
        id: 5,
        title: '5 Why the use of metal dishes is much more useful for health?',
        img: require('../img/uploads/1.png'),
        date: '09.07.2018',
        desc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco laboris nisi ut aliquip ex `,
        sumLikes: 10,
        sumComments: 123,
        like: true

    }, {
        id: 6,
        title: '6 Why the use of metal dishes is much more useful for health?',
        img: require('../img/uploads/1.png'),
        date: '09.07.2018',
        desc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                             tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                             quis nostrud exercitation ullamco laboris nisi ut aliquip ex `,
        sumLikes: 95,
        sumComments: 12,
        like: false

    }
];
let blogData = {
    next: 'url',
    prev: 'url',
    data: posts
};
export default blogData;
