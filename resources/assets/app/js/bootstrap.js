import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import VueAgile from 'vue-agile'

import axios from 'axios';
import moment from 'moment';
import lodash from 'lodash';
import VueCarousel from 'vue-carousel';


window.moment = moment;
window.axios = axios;
//window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';



window._ = lodash;



Vue.use(BootstrapVue)
    .use(VueAgile)
    .use(VueCarousel);
