let params = {
    hotProducts: [
        {
            id: 1,
            name: 'fittings',
            img: require('../img/uploads/2.png'),
            price: 90

        },
        {
            id: 2,
            name: 'cutlery',
            img: require('../img/uploads/1.png'),
            price: 180

        }

    ],
    hotNews: [
        {
            id: 1,
            title: 'Why the use of metal dishes is much more useful for health?',
            date: '09.07.2018',
            img: require('../img/uploads/1.png'),
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`
        },
        {
            id: 2,
            title: 'Why the use of metal dishes is much more useful for health?',
            date: '09.07.2018',
            img: require('../img/uploads/1.png'),
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`
        },
        {
            id:3,
            title: 'Why the use of metal dishes is much more useful for health?',
            date: '09.07.2018',
            img: require('../img/uploads/1.png'),
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`
        }
    ]
};
export default params;
