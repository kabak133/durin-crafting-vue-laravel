import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

// Admin pages
import Overview from '../components/Dashboard/Views/Overview.vue'
import UserProfile from '../components/Dashboard/Views/UserProfile.vue'
import Notifications from '../components/Dashboard/Views/Notifications.vue'
import Icons from '../components/Dashboard/Views/Icons.vue'
import Maps from '../components/Dashboard/Views/Maps.vue'
import Typography from '../components/Dashboard/Views/Typography.vue'
import TableList from '../components/Dashboard/Views/TableList.vue'
import LoginPage from '../views/LoginPage.vue'
import Blog from '../views/Blog/ArchivePosts.vue'
import AddPost from '../views/Blog/PostAdd'
import PostEdit from '../views/Blog/PostEdit'
import ProductsShow from '../views/Products/ProductShow'
import ProductAdd from '../views/Products/ProductAdd'
import ProductEdit from '../views/Products/ProductEdit'
import ContactUSMessages from '../views/ContactUSMessages/MessagesList'
import MessageReply from '../views/ContactUSMessages/MessageReply'

const routes = [
    {
        path: '/',
        name: 'login',
        component: LoginPage,
    },
    {
        path: '/admin',
        component: DashboardLayout,
        redirect: '/admin/stats',
        children: [
            {
                path: '/overview',
                name: 'overview',
                component: Overview
            },
            {
                path: '/stats',
                name: 'stats',
                component: UserProfile
            },
            {
                path: '/notifications',
                name: 'notifications',
                component: Notifications
            },
            {
                path: '/icons',
                name: 'icons',
                component: Icons
            },
            {
                path: '/maps',
                name: 'maps',
                component: Maps
            },
            {
                path: '/typography',
                name: 'typography',
                component: Typography
            },
            {
                path: '/table-list',
                name: 'table-list',
                component: TableList
            },
            {
                path: '/blog',
                name: 'blog',
                component: Blog
            },
            {
                path: '/blog/post/add',
                name: 'addPost',
                component: AddPost
            },
            {
                path: '/blog/post/edit',
                name: 'editPost',
                component: PostEdit,
                props: {post: null}
            },
            {
                path: '/products',
                name: 'products',
                component: ProductsShow,
            },
            {
                path: '/products/add',
                name: 'prodAdd',
                component: ProductAdd
            },
            {
                path: '/products/edit',
                name: 'prodEdit',
                component: ProductEdit
            },
            {
                path: '/messages',
                name: 'messages',
                component: ContactUSMessages
            },
            {
                path: '/messages/reply/:id',
                name: 'messageReply',
                component: MessageReply
            },
        ]
    },
    { path: '*', component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
