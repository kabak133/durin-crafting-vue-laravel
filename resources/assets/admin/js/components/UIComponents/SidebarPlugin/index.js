import Sidebar from './SideBar.vue'

const SidebarStore = {
    showSidebar: false,
    sidebarLinks: [
        {
            name: 'Blog',
            icon: 'ti-write',
            path: 'blog'
        },
        {
            name: 'Products',
            icon: 'ti-shopping-cart-full',
            path: 'products'
        },
        {
            name: 'Messages',
            icon: 'ti-email',
            path: 'messages'
        },
        {
            name: 'Dashboard',
            icon: 'ti-panel1',
            path: 'overview'
        },
        {
            name: 'User Profile',
            icon: 'ti-user1',
            path: 'stats'
        },
        {
            name: 'Table List',
            icon: 'ti-view-list-alt1',
            path: 'table-list'
        },
        {
            name: 'Typography',
            icon: 'ti-text1',
            path: 'typography'
        },
        {
            name: 'Icons',
            icon: 'ti-pencil-alt1',
            path: 'icons'
        },
        {
            name: 'Maps',
            icon: 'ti-map1',
            path: 'maps'
        },
        {
            name: 'Notifications',
            icon: 'ti-bell1',
            path: 'notifications'
        }
    ],
    displaySidebar(value) {
        this.showSidebar = value
    }
}

const SidebarPlugin = {

    install(Vue) {
        Vue.mixin({
            data() {
                return {
                    sidebarStore: SidebarStore
                }
            }
        })

        Object.defineProperty(Vue.prototype, '$sidebar', {
            get() {
                return this.$root.sidebarStore
            }
        })
        Vue.component('side-bar', Sidebar)
    }
}

export default SidebarPlugin
