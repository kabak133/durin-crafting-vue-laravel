export default {
    methods: {
        AuthHeaders() {
            return {
                headers: {
                    Authorization: 'Bearer ' + this.$storage.get('token')
                }
            }
        }
    },
}
