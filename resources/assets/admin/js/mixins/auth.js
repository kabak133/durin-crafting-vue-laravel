import AuthHeaders from './auth-headers';

export default {
    mixins: [AuthHeaders],
    data() {
        return {
            auth: false,
        };
    },
    methods: {
        checkAuth() {
            let status
            console.log('has token:', this.$storage.has('token'))

            console.log('this.AuthHeaders:', this.AuthHeaders())
            if (this.$storage.has('token')) {
                axios.get(
                    '/api/auth/check',
                    this.AuthHeaders(),
                ).then((resp) => {
                   // console.log(resp)
                    if(!resp.data.auth){
                        this.$router.push({name: 'login'})
                        this.$storage.remove('token');
                        status = 'faild'
                    }else{
                        status = 'ok'
                    }
                }).catch((err) => {
                    console.dir(err)
                   this.$storage.remove('token');
                });
            }
            else {
                status = 'faild'
                this.$router.push({name: 'login'})
            }
        },
        deauthorize() {
            axios.get(
               'api/auth/logout',
                this.AuthHeaders(),
            ).then((r) => {
                this.$storage.remove('token');
                this.$router.push('login')
            }).catch((err)=>{
                console.dir(err)
            });

        }
    }
};
