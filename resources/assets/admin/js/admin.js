import Vue from 'vue'
import VueRouter from 'vue-router'
import vClickOutside from 'v-click-outside'

// Plugins
import GlobalComponents from './gloablComponents'
import Notifications from './components/UIComponents/NotificationPlugin'
import SideBar from './components/UIComponents/SidebarPlugin'
import App from './App.vue'

// router setup
import routes from './router/router'

// library imports
import Chartist from 'chartist'
import BootstrapVue from 'bootstrap-vue';

import axios from 'axios';
import moment from 'moment';
import Vue2Storage from 'vue2-storage'
import Datatable from 'vue2-datatable-component'
import VeeValidate from 'vee-validate';


import lodash from 'lodash'

// Advanced Use - Hook into Quill's API for Custom Functionality
import {VueEditor, Quill} from "vue2-editor";


window.moment = moment;
window.axios = axios;
window._ = lodash;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


// plugin setup
Vue.use(VueRouter)
   .use(GlobalComponents)
   .use(vClickOutside)
   .use(Notifications)
   .use(BootstrapVue)
   .use(SideBar)
   .use(Vue2Storage)
   .use(Datatable)
   .use(VueEditor)
   .use(VeeValidate)

// configure router
const router = new VueRouter({
    routes, // short for routes: routes
    linkActiveClass: 'active'
});

// global library setup
Object.defineProperty(Vue.prototype, '$Chartist', {
    get() {
        return this.$root.Chartist
    }
})

new Vue({
    el: '#app',
    render: h => h(App),
    router,
    data: {
        Chartist: Chartist
    }
})
