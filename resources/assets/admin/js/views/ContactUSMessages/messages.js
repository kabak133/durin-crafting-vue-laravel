let messages =  [
    {
        id: 1,
        name: 'Grisha',
        email: 'grisha@gmail.com',
        message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam aliquid aperiam est eum eveniet id iure saepe ut veniam?',
        date: '03.09.2018 12:37',
        status: 'read'
    },
    {
        id: 2,
        name: 'Misha',
        email: 'misha@gmail.com',
        message: 'Lorem ipsum dolor sit amet, consectetur.  A aliquam aliquid aperiam est eum eveniet id iure saepe ut veniam? adipisicing elit. A aliquam aliquid aperiam est eum eveniet id iure saepe ut veniam?',
        date: '03.09.2018 13:41',
        status: 'unread'
    },
    {
        id: 3,
        name: 'Gosha',
        email: 'gosha@gmail.com',
        message: 'Lorem ipsum dolor sit amet, consectetur.  A aliquam aliquam aliquid aperiam est eum eveniet id iure saepe ut veniam?',
        date: '03.09.2018 14:55',
        status: 'unread'
    },
    {
        id: 4,
        name: 'Masha',
        email: 'mahsa@gmail.com',
        message: 'Lorem ipsum dolor sit amet, consectetur.  A aliquam aliquam aliquid aperiam est eum am aliquam aliquid aperiam est eum eveniet id iure saepe ut veniam?',
        date: '03.09.2018 15:55',
        status: 'read'
    }
];
export default messages;