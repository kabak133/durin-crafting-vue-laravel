<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/static/favicon.png"/>
    <title>Durin - Crafting</title>


    <!-- Styles -->
    <link href="{{asset('css/'.(Request::is('admin*')?'admin':'app').'.css')}}" rel="stylesheet" type="text/css">

</head>
<body>
<div id="app"></div>


<script src={{asset('js/'.(Request::is('admin*')?'admin':'app').'.js')}}></script>
{{--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1RTtf-i6UOIXfqtDplHGqkGyiXPErE6E"
        type="text/javascript"></script>--}}
</body>
</html>
