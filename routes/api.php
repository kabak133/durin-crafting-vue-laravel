<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('User')->prefix('auth')->group(function (){
    Route::post('/register', 'AuthController@register')->name('register');
    Route::post('/login', 'AuthController@login')->name('login');
    Route::get('/check', 'AuthController@checkAuthUser')->name('check');

    Route::middleware('jwt.auth')->group(function (){
        Route::get('/logout', 'AuthController@logout')->name('logout');

    });
});

Route::prefix('front')->group(function (){

    Route::middleware('auth.jwt')->group(function (){
        Route::post('/products/{product}/rating', 'FrontController@saveRatingToProduct')->name('save.rating');
        Route::get('/posts/{post}/like', 'FrontController@addLikeToPost')->name('add.like');
        Route::post('/{type}/{id}/comment', 'CommentsController')->name('add.comment');

    });

    Route::get('/posts', 'FrontController@posts')->name('posts');
    Route::get('/products', 'FrontController@products')->name('products');
    Route::get('/posts/{post}', 'FrontController@showPost')->name('post');
    Route::get('/products/{product}', 'FrontController@showProduct')->name('product');
    Route::post('/message', 'MessagesController')->name('create.message');

});


