<?php
//
///*
//|--------------------------------------------------------------------------
//| Web Routes
//|--------------------------------------------------------------------------
//|
//| Here is where you can register web router for your application. These
//| router are loaded by the RouteServiceProvider within a group which
//| contains the "web" middleware group. Now create something great!
//|
//*/
//
//Route::get('/', function () {
//    return view('index');
//});
//
////Auth::router();
//
////Route::get('/home', 'HomeController@index')->name('home');
//
//Route::get('/admin', function (){
//    return view ('admin');
//});
Route::view('/{all}', 'app')->where('all', '.*');
