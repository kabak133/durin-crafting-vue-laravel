<?php

Route::apiResource('posts', 'PostsController', ['except' => 'show']);
Route::delete('products/{product_id}/image/delete/{image_id}', 'ProductsController@deleteImage')->name('Products.delete.image');
Route::apiResource('products', 'ProductsController', ['except' => 'show']);
Route::get('users', 'UserController')->name('users.index');
Route::post('messages/{message}/reply', 'MessagesController@reply')->name('messages.reply');
Route::apiResource('messages', 'MessagesController', ['except' => 'store']);