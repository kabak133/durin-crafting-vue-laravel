<?php

namespace App\Services;

use JWTAuth;

class JWTService
{
    public function checkAuthUser()
    {
        $token = JWTAuth::parseToken();

        if($token){
            $user = JWTAuth::toUser($token);

            return $user;
        }

        return false;
    }

    public function getAuthUser(string $token)
    {
        $user = JWTAuth::toUser($token);

        return $user;
    }
}