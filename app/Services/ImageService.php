<?php

namespace App\Services;

use App\Models\ProductImage;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;

class ImageService
{
    /**
     * @param int $id
     * @param array $images
     */
    public function saveImageByProductId(int $id, array $images) : void
    {
        foreach ($images as $image) {
            $data = [
                'product_id' => $id,
                'image' => $image
            ];

            ProductImage::create($data);
        }
    }

    /**
     * @param Collection $images
     */
    public function deleteImagesByProductId(Collection $images) : void
    {
        foreach ($images as $image){

            Storage::disk('products')->delete([$image->image, $image->crop_image]);

            ProductImage::destroy($image->id);
        }
    }

    /**
     * @param int $product_id
     * @param int $image_id
     * @param ProductImage $image
     * @return bool|null
     * @throws \Exception
     */
    public function deleteImageById(int $product_id, int $image_id)
    {
        $image = ProductImage::where('product_id', $product_id)->where('id', $image_id)->firstOrFail();

        Storage::disk('products')->delete($image->image);

        return ProductImage::destroy($image_id);

    }

}
