<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReplyClient extends Mailable
{
    use Queueable, SerializesModels;

    public $reply;
    /**
     * Create a new message instance.
     *
     * @param $replyData
     * @return void
     */
    public function __construct($replyData)
    {
        $this->reply = $replyData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.client_reply')
                    ->with([
                        'text' => $this->reply
                    ]);
    }
}
