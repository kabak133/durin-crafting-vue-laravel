<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'is_admin', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function comments()
    {
        return $this->hasMany(Comment::class, 'user_id');
    }

    public function likes()
    {
        return $this->belongsToMany(
            Like::class, 'likes', 'user_id', 'post_id');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'user_id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'user_id');
    }

    /**
     *  Get full path to avatar
     *
     * @return string
     */
    public function getAvatarLinkAttribute()
    {
        $avatar = $this->attributes['avatar'];

        if($avatar){
            return 'avatars/'.$avatar;
        }

        return $avatar;
    }

    public function setAvatarAttribute($value)
    {
        $disk = Storage::disk('avatars');

        if (isset($this->attributes['avatar']) && $disk->exists($this->attributes['avatar'])) {
            $disk->delete($this->attributes['avatar']);
        }
        $name = uniqid() . '.' . $value->getClientOriginalExtension();

        $disk->put($name, File::get($value));

        $this->attributes['avatar'] = $name;
    }

    /**
     * Get full name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
