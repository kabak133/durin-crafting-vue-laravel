<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class ProductImage extends Model
{
    protected $table = 'product_images';

    protected $fillable = [
        'product_id', 'image', 'main'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * Set the image.
     *
     * @param  file  image
     *
     * @return void
     */
    public function setImageAttribute($image)
    {
        $disk = Storage::disk('products');

        $name = uniqid() . '.' . $image->getClientOriginalExtension();

        $crop_image = Image::make($image)->resize(300, 200)->encode('jpg');

        $disk->put($name, File::get($image));
        $disk->put('crop-'.$name, $crop_image);

        $this->attributes['image'] = $name;
        $this->attributes['crop_image'] = 'crop-'.$name;
    }
}
