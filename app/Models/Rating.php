<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'ratings';

    protected $fillable = [
        'rating', 'product_id', 'user_id'
    ];

    public function getAverageAttribute($value)
    {
        return round($value, 1, PHP_ROUND_HALF_UP);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}