<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'price', 'description', 'add_info', 'image', 'property', 'short_description', 'user_id', 'link'
    ];

    /**
     * Relation with comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @return array|string
     */
    public function getImagesLinksAttribute()
    {
        $links = [];

        foreach ($this->images as $image){
            $links[$image->id] = [
                'image' =>'/images/products/'.$image->image,
                'crop_image' =>'/images/products/crop-'.$image->image,
                'id' => $image->id
            ];
        }

        if(empty($links)){
            return $links[] = ['/default_images/product.png'];
        }
        return $links;
    }

    /**
     * Get average sum of like
     *
     * @return float|string
     */
    public function getAverageRatingAttribute()
    {
        $total = count($this->ratings);
        $sum = 0;

        foreach ($this->ratings as $rating){
            $sum = $sum + $rating->rating;
        }

        if($sum) {
            $average = round($sum/$total, 1);

            return $average;
        }

        return '';
    }

}
