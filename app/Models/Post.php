<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class Post extends Model
{
    protected $fillable = [
        'title', 'body', 'image', 'user_id', 'crop_image'
    ];

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class, 'post_id');
    }

    /**
     * Get the image.
     *
     *
     * @return string|array
     */
//    public function getImageLinkAttribute()
//    {
//        if (empty($this->attributes['image'])) {
//            return '/default_images/post.png';
//        }
//        return [
//                 'image' => '/posts/' . $this->attributes['image'],
//            'crop_image' => '/posts/crop-' . $this->attributes['image']
//        ];
//    }
    /**
     * Set the image.
     *
     * @param  file  $image
     * @return void
     */
    public function setImageAttribute($image)
    {
        $disk = Storage::disk('posts');

        if (isset($this->attributes['image']) && $disk->exists($this->attributes['image'])) {
            $disk->delete($this->attributes['image']);
        }

        $name = uniqid() . '.' . $image->getClientOriginalExtension();

        $crop_image = Image::make($image)->resize(300, 200)->encode('jpg');

        $disk->put($name, File::get($image));
        $disk->put('crop-'.$name, $crop_image);

        $this->attributes['image'] = $name;
        $this->attributes['crop_image'] = 'crop-'.$name;
    }

}
