<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use JWTAuth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user &&  $user->is_admin) {
            return $next($request);
        } else {
            throw new AccessDeniedHttpException();
        }
    }
}
