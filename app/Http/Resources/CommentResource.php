<?php
namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class CommentResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
              'id' => $this->id,
            'body' => $this->body,
            'date' => $this->updated_at->format('Y-m-d H:s'),
            'user' => $this->user->name
        ];
    }
}
