<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                     'id' => $this->id,
                   'name' => $this->name,
                  'price' => $this->price,
                   'date' => $this->updated_at->format('Y-m-d H:s'),
                 'images' => $this->images_links,
            'description' => $this->description,
               'add_info' => $this->add_info,
      'short_description' => $this->short_description,
               'property' => $this->property,
               'comments' => CommentResource::collection($this->comments),
                   'link' => $this->link,
         'average_rating' => $this->average_rating,
                   'user' => $this->user->name,
            //TODO: Add to db , logic
                    'sale'=> rand(0, 1) == 1 ? false : true,
            'category_id' => rand(1,3 ),
                'oldPrice'=> 100,
                    $this->mergeWhen($request->is('api/front/products/*'), [
                        'related' => Product::where('id', '!=', $this->id)->get()->random(4)
                    ]),
        ];
    }
}
