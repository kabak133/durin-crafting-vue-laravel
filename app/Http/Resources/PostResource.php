<?php

namespace App\Http\Resources;

use App\Models\Comment;
use Illuminate\Http\Resources\Json\Resource;

class PostResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                  'id' => $this->id,
               'title' => $this->title,
                'body' => $this->body,
            'comments' => CommentResource::collection($this->comments),
//            'comments' => ,
        'sum_comments' => count($this->comments),
               'likes' => count($this->likes),
               'image' => '/images/posts/'. $this->image,
               'crop_image' => '/images/posts/crop-'. $this->image,
                'date' => $this->updated_at->format('Y-m-d H:s'),
                'user' => $this->user->name,
        ];
    }
}
