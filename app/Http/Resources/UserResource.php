<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                   'name' => $this->name,
                  'email' => $this->email,
                 'avatar' => $this->avatar_link,
            'register_at' => $this->created_at->format('Y-m-d H:s')
        ];
    }
}
