<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as Request;
use Illuminate\Validation\Rule;

class AuthRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email = ['required', 'email', 'unique:users,email'];

        if ($this->method() == 'PUT') {
            $user_id = $this->route('user');
            $email = ['required', 'email', Rule::unique('users')->ignore($user_id)];
        }

        return [
            'first_name' => ['required', 'string'],
             'last_name' => ['required', 'string'],
              'password' => ['required', 'string', 'min:6'],
                 'email' => $email,
                'avatar' => ['sometimes', 'required', 'mimes:png,jpeg,bmp', 'max:5000'],
              'is_admin' => ['sometimes', 'boolean']
        ];
    }
}