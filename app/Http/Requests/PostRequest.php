<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as Request;
use Illuminate\Validation\Rule;

class PostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $title = ['required', 'string', 'unique:posts,title'];

        $rules = [];
        $rules['image'] = ['sometimes', 'required', 'mimes:png,jpeg,bmp'];

        if ($this->method() == 'PATCH') {
            $post_id = $this->route('post');
            $title = ['required', 'string', Rule::unique('posts')->ignore($post_id)];

            if(is_string($this['image']) ){
                unset($this['image']);
                unset($rules['image']);
            }
        }

        $rules['title'] = $title;

        return $rules;

    }
}
