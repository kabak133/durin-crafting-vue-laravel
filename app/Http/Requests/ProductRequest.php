<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as Request;
use Illuminate\Validation\Rule;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $name = ['required', 'string', 'unique:products,name'];

        if ($this->method() == 'PATCH') {
            $product_id = $this->route('product');
            $name = ['required', 'string', Rule::unique('Products')->ignore($product_id)];
        }

        $price = ['required', 'integer'];

        if(is_numeric($this->image)){
            $price = ['required'];
        }

        return [

            'name' => $name,
            'price' => $price,
            'images.*' => ['sometimes', 'required', 'mimes:png,jpeg,jpg,bmp']
        ];
    }
}
