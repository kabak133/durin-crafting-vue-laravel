<?php
namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Services\JWTService;
use Illuminate\Http\JsonResponse;

class CommentsController extends Controller
{
    public function __invoke(
        string $type,
        int $id,
        CommentRequest $request,
        JWTService $JWTService
    )
    {
        if($user = $JWTService->checkAuthUser()){

            $model = 'App\Models\\'.$type;

            $entity = $model::find($id);

            $comment = new Comment;
            $comment->body = $request->comment;
            $comment->user_id = $user->id;

            $entity->comments()->save($comment);

            return new JsonResponse(null, 204);

        };

        return new JsonResponse(['success' => false, 'message' => 'User is not authenticated!']);
    }
}