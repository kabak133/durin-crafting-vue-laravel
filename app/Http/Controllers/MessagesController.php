<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use App\Mail\CustomerRequest;
use App\Models\Message;
use Illuminate\Support\Facades\Mail;

class MessagesController extends Controller
{
    /**
     * Send and save new message in DB
     *
     * @param MessageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(MessageRequest $request)
    {
        $request['status'] = config('project.message_status.new');

        $message = Message::create($request->all());

        Mail::to(config('project.mail_receiver'))->send(new CustomerRequest($message));

        return response()->json(null, 204);
    }
}
