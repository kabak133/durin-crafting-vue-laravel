<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @param AuthRequest $request
     * @return JsonResponse
     */
    public function register(AuthRequest $request)
    {
        $request['password'] = Hash::make($request['password']);

        $user = User::create($request->all());

        return new JsonResponse(['status' => 'success', 'data' => $user], 200);

    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentails = $request->only('email', 'password');

        if(!$jwt_token = JWTAuth::attempt($credentails)){
            return new JsonResponse([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }

        return new JsonResponse(['success' => true , 'token' => $jwt_token], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        $token = $request->header('Authorization');

        try {
            JWTAuth::invalidate($token);

            return new JsonResponse([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function checkAuthUser(Request $request)
    {
//        return new JsonResponse(['data' => $request->header('Authorization')]);

        if($token = $request->header('Authorization')){

            try {
                JWTAuth::parseToken()->authenticate();

                return new JsonResponse(['auth' => true]);
            } catch (JWTException $exception) {
                return new JsonResponse([
                    'auth' => false,
                    'message' => 'Sorry, the user did not find'
                ], 500);
            }

        };

        return new JsonResponse(['auth' => false]);

    }
}
