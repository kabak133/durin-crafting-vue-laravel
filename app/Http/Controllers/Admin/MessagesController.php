<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReplyRequest;
use App\Http\Resources\MessageResource;
use App\Mail\ReplyClient;
use App\Models\Message;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return MessageResource::collection(Message::all());
    }

    /**
     * Display the specified resource.
     *
     * @param Message $message
     * @return MessageResource
     */
    public function show(Message $message)
    {
        return new MessageResource($message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ReplyRequest $request
     * @param Message $message
     * @return \Illuminate\Http\Response
     */
    public function reply(ReplyRequest $request, Message $message)
    {
//        return response()->json([$request->all()], 200);
        Mail::to($request->email)->send(new ReplyClient($request->message));

        $message->update(['status' => config('project.message_status.reply')]);

        return response()->json(null, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Message $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
