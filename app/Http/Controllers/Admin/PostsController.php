<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Services\JWTService;
use Illuminate\Http\JsonResponse;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->limit ?? 10;
        $offset = $request->offset ?? 0;
        $sort = $request->sort ?? 'updated_at';
        $order = $request->order ?? 'desc';

        if($sort == 'date'){
            $sort = 'updated_at';
        }

        if($sort == 'user'){
            $posts =  PostResource::collection(
                Post::join('users', 'user_id', '=', 'users.id')
                    ->where('users.is_admin', 1)
                    ->select('posts.*')
                    ->orderBy('users.last_name', $order)
                    ->orderBy('updated_at', $order)
                    ->with(['comments', 'likes', 'user'])
                    ->limit($limit)
                    ->offset($offset)
                    ->get()
            );
        }else {
            $posts = PostResource::collection(
                Post::with(['comments', 'likes', 'user'])
                    ->orderBy($sort, $order)
                    ->limit($limit)
                    ->offset($offset)
                    ->get()
            );
        }

        $total = Post::count();

        return new JsonResponse(['data' => $posts, 'total' => $total]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param JWTService $service
     * @param PostRequest $request
     * @return PostResource
     */
    public function store(PostRequest $request, JWTService $service)
    {
        $user = $service->getAuthUser($request->header('Authorization'));

        $request['user_id'] = $user->id;

        $post = Post::create($request->all());

        return new PostResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param JWTService $service
     * @param PostRequest $request
     * @param Post $post
     * @return PostResource
     */
    public function update(PostRequest $request, Post $post, JWTService $service)
    {
        $user = $service->getAuthUser($request->header('Authorization'));

//        $request['user_id'] = $user->id;

        $post->update($request->all());

        return new PostResource($post);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        $post->comments()->delete();

        $post->likes()->delete();

        Storage::disk('posts')->delete([$post->image, $post->crop_image]);

        $post->delete();

        return new JsonResponse(null, 204);
    }
}
