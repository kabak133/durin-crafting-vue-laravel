<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\ImageService;
use App\Services\JWTService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->limit ?? 10;
        $offset = $request->offset ?? 0;
        $sort = $request->sort ?? 'updated_at';
        $order = $request->order ?? 'desc';

        if($sort == 'date'){
            $sort = 'updated_at';
        }

        if($sort == 'user'){
            $products =  ProductResource::collection(
                Product::join('users', 'user_id', '=', 'users.id')
                    ->where('users.is_admin', 1)
                    ->select('Products.*')
                    ->orderBy('users.last_name', $order)
                    ->orderBy('updated_at', $order)
                    ->with(['comments', 'ratings', 'images', 'user'])
                    ->limit($limit)
                    ->offset($offset)
                    ->get()
            );
        }else {
            $products = ProductResource::collection(
                Product::with(['comments', 'ratings', 'images','user'])
                    ->orderBy($sort, $order)
                    ->limit($limit)
                    ->offset($offset)
                    ->get()
            );
        }

        $total = Product::count();

        return new JsonResponse(['data' => $products, 'total' => $total]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param JWTService $service
     * @param ImageService $imageService
     * @param ProductRequest $request
     * @return ProductResource
     */
    public function store(ProductRequest $request, ImageService $imageService, JWTService $service)
    {
        $user = $service->getAuthUser($request->header('Authorization'));

        $request['user_id'] = $user->id;

        $product = Product::create($request->all());

        if($request->hasFile('images')){
            $imageService->saveImageByProductId($product->id, $request['images']);
        }

        return new ProductResource($product);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param JWTService $service
     * @param ImageService $imageService
     * @param ProductRequest $request
     * @param Product $product
     * @return ProductResource
     */
    public function update(
        ProductRequest $request,
        Product $product,
        ImageService $imageService,
        JWTService $service
    )
    {
        $user = $service->getAuthUser($request->header('Authorization'));

        $request['user_id'] = $user->id;

        $product->update($request->all());

        if($request->hasFile('images')){
            $imageService->saveImageByProductId($product->id, $request['images']);
        }

        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ImageService $imageService
     * @param Product $product
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Product $product, ImageService $imageService)
    {
        $product->comments()->delete();

        $product->ratings()->delete();

        $imageService->deleteImagesByProductId($product->images);

        $product->delete();

        return new JsonResponse(null, 204);
    }

    /**
     * @param int $product_id
     * @param int $image_id
     * @param ImageService $imageService
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteImage(int $product_id, int $image_id, ImageService $imageService)
    {
        $imageService->deleteImageById($product_id, $image_id);

        return new JsonResponse(null, 204);
    }
}
