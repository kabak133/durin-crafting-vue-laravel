<?php

namespace App\Http\Controllers;

use App\Http\Requests\RatingRequest;
use App\Http\Resources\ProductResource;
use App\Models\Post;
use App\Models\Product;
use App\Models\Rating;
use App\Models\Like;
use Illuminate\Http\Request;
use JWTAuth;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\PostResource;

class FrontController extends Controller
{
    /**
     * Return all posts
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function posts()
    {
        return PostResource::collection(Post::with('comments', 'likes')->get());
    }

    /**
     * Return all Products
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function products()
    {
        return ProductResource::collection(Product::with('comments', 'ratings', 'images')->get());
    }

    /**
     * Return a post by ID
     *
     * @param Post $post
     * @return PostResource
     */
    public function showPost(Post $post)
    {
        return new PostResource($post);
    }

    /**
     * Return a product by ID
     *
     * @param Product $product
     * @return ProductResource
     */
    public function showProduct(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Save product's rating
     *
     * @param Product $product
     * @param RatingRequest $request
     * @return JsonResponse
     */
    public function saveRatingToProduct(Product $product, RatingRequest $request)
    {
        if($token = $request->header('Authorization')){
            $user = JWTAuth::toUser($token);

            $data = [
                'rating' => $request->rating,
            ];

            if(!Rating::updateOrCreate(
                ['product_id' => $product->id, 'user_id' => $user->id], $data)){
                return new JsonResponse(null, 500);
            };

            return new JsonResponse(null, 200);
        };

    }

    /**
     *
     *
     * @param Post $post
     * @param Request $request
     * @return JsonResponse
     */
    public function addLikeToPost(Post $post, Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));

        $existing_like = Like::withTrashed()->wherePostId($post->id)->whereUserId($user->id)->first();

        if (is_null($existing_like)) {
            Like::create([
                'like' => 1,
                'post_id' => $post->id,
                'user_id' => $user->id
            ]);
        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
            } else {
                $existing_like->restore();
            }
        }

        return new JsonResponse(null, 204);
    }

}
