let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (mix.inProduction()) {
    mix.version();
} else {
    mix.webpackConfig({devtool: "inline-source-map"}).sourceMaps();
}

mix.js('resources/assets/app/js/app.js', 'public/js')
   .sass('resources/assets/app/scss/app.scss', 'public/css')

   .js('resources/assets/admin/js/admin.js', 'public/js')
   .sass('resources/assets/admin/scss/admin.scss', 'public/css')
